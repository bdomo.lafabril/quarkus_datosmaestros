package com.lafabril.ec.dao;

import com.lafabril.ec.domain.BancoCuenta;

import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;

/**
 *
 * @author kmuniz
 */
@ApplicationScoped
public class DaoDatosMaestros {
    
    @Inject
    EntityManager em;
    
    @Transactional
    public List<Object[]> getAllDatos(){
        Query query = em.createNativeQuery("SELECT a.TIPO_TRANS, a.DOC_TRANS, a.LINE_TRANS, a.TIPO_DOC, a.NUM_DOC, a.LINE_DOC, "
                    + "a.ANIO, a.PERIODO, a.FECHA_DOC, a.FECHA_VENC, 'Pendiente cvta' t$cvat, a.IMPORTE, a.VALOR_IVA, "
                    + "a.NUMERO_ASIENTO, a.SALDO, a.TIPO, a.ID_CLIENTE, b.NOMBRE_CLIENTE1, b.RUC_CLIENTE, d.DSCA_CANTON, a.DOC_SRI "
                    + "FROM DATASAP.DOCUMENTO_PAGO a "
                    + "INNER JOIN DATASAP.CLIENTE b ON b.ID_CLIENTE = a.ID_CLIENTE "
                    + "INNER JOIN DATASAP.CLIENTE_VENTA_AMPLIADO h ON h.ID_CLIENTE = b.ID_CLIENTE "
                    + "INNER JOIN DATASAP.EMPRESA j ON j.ID_EMPRESA = a.ID_SOCIEDAD "
                    + "LEFT JOIN DATASAP.CANTON d ON d.ID_PROVINCIA = b.ID_PROVINCIA  AND d.ID_CANTON = b.ID_CANTON "
                    + "INNER JOIN SUIFAB.TIPO_DOCUMENTO e ON e.TIPO_TRANS = a.TIPO_TRANS "
                    + "WHERE a.NUM_DOC = 0");
        return query.getResultList();
    }

    public BancoCuenta find(){
        return  em.createQuery("", BancoCuenta.class).getSingleResult();
    }
    
}
