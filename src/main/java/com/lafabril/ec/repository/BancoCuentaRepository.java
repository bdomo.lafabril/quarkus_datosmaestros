package com.lafabril.ec.repository;

import com.lafabril.ec.domain.BancoCuenta;
import io.quarkus.hibernate.orm.panache.PanacheRepository;
import java.util.List;
import javax.enterprise.context.ApplicationScoped;

/**
 *
 * @author kmuniz
 */
@ApplicationScoped
public class BancoCuentaRepository implements PanacheRepository<BancoCuenta>{
    
    public List<BancoCuenta> findIdCuenta(String idCuenta){
        return list("idCuenta", idCuenta);
    }
    
}
