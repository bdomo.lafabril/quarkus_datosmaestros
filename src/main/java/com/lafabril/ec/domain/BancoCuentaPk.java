package com.lafabril.ec.domain;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author kmuniz
 */
public class BancoCuentaPk implements Serializable {
    
    private String idEmpresa;
    private String idBanco;
    private String idCuenta;

    public BancoCuentaPk() {
    }

    public String getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(String idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public String getIdBanco() {
        return idBanco;
    }

    public void setIdBanco(String idBanco) {
        this.idBanco = idBanco;
    }

    public String getIdCuenta() {
        return idCuenta;
    }

    public void setIdCuenta(String idCuenta) {
        this.idCuenta = idCuenta;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + Objects.hashCode(this.idEmpresa);
        hash = 79 * hash + Objects.hashCode(this.idBanco);
        hash = 79 * hash + Objects.hashCode(this.idCuenta);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final BancoCuentaPk other = (BancoCuentaPk) obj;
        if (!Objects.equals(this.idEmpresa, other.idEmpresa)) {
            return false;
        }
        if (!Objects.equals(this.idBanco, other.idBanco)) {
            return false;
        }
        return Objects.equals(this.idCuenta, other.idCuenta);
    }
    
}
