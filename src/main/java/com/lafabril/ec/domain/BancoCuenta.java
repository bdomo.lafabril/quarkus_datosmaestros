package com.lafabril.ec.domain;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author kmuniz
 */

@Entity
@Table(name = "banco_cuenta", schema = "datasap")
@IdClass(BancoCuentaPk.class)
public class BancoCuenta implements Serializable{
    
    @Id
    @NotNull
    @Column(name = "id_empresa")
    private String idEmpresa;
    
    @Id
    @NotNull
    @Column(name = "id_banco")
    private String idBanco;
    
    @Id
    @NotNull
    @Column(name = "id_cuenta")
    private String idCuenta;
    
    @Basic
    @Column(name = "cta_bancaria")
    private String cuentaBancaria;
    
    @Basic
    @Column(name = "clave_control")
    private String claveControl;
    
    @Basic
    @Column(name = "moneda")
    private String moneda;
    
    @Basic
    @Column(name = "cta_libro_mayor")
    private String cuentaLibroMayor;
    
    @Basic
    @Column(name = "status")
    private boolean status;

    public BancoCuenta() {
    }

    public String getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(String idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public String getIdBanco() {
        return idBanco;
    }

    public void setIdBanco(String idBanco) {
        this.idBanco = idBanco;
    }

    public String getIdCuenta() {
        return idCuenta;
    }

    public void setIdCuenta(String idCuenta) {
        this.idCuenta = idCuenta;
    }

    public String getCuentaBancaria() {
        return cuentaBancaria;
    }

    public void setCuentaBancaria(String cuentaBancaria) {
        this.cuentaBancaria = cuentaBancaria;
    }

    public String getClaveControl() {
        return claveControl;
    }

    public void setClaveControl(String claveControl) {
        this.claveControl = claveControl;
    }

    public String getMoneda() {
        return moneda;
    }

    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    public String getCuentaLibroMayor() {
        return cuentaLibroMayor;
    }

    public void setCuentaLibroMayor(String cuentaLibroMayor) {
        this.cuentaLibroMayor = cuentaLibroMayor;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
    
}
