package com.lafabril.ec.security;

/**
 * Constants for Security authorities.
 */
public final class AuthoritiesConstants {
    /*Roles Globales*/
    public static final String ADMIN = "ROLE_ADMIN";
    public static final String USER = "ROLE_USER";
    public static final String ANONYMOUS = "ROLE_ANONYMOUS";
    
    /*Roles Aplicacion PaperLess*/
    public static final String PAPER_OPERADOR = "ROLE_PAPER_OPERADOR";
    public static final String PAPER_TRANSPORTISTA = "ROLE_PAPER_TRANSPORTISTA";
    public static final String PAPER_GARITA = "ROLE_PAPER_GARITA";
    public static final String PAPER_CONSULTOR = "ROLE_PAPER_COORDINADOR";
    
    /*Roles Aplicaciones Dotacion*/
    public static final String ADMIN_DOTACION = "ROLE_ADMIN_DOTACION";
    
    private AuthoritiesConstants() {
    }
    
}
