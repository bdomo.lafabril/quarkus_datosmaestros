package com.lafabril.ec.controller;

import static com.lafabril.ec.security.AuthoritiesConstants.*;
import com.lafabril.ec.dao.DaoDatosMaestros;
import com.lafabril.ec.repository.BancoCuentaRepository;
import java.util.List;
import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author kmuniz
 */
@Path("/api/banco-cuenta")
@Produces(MediaType.APPLICATION_JSON)
@RequestScoped 
public class BancoCuentaController {
    
    @Inject
    BancoCuentaRepository bCtaRepos;
    
    @Inject
    DaoDatosMaestros daoDatosMaestros;
     
    @GET
    @RolesAllowed(USER)
    public Response getAllBancoCuenta() {
        return Response.ok(bCtaRepos.findAll().list()).build();
    }
    
    @GET
    @RolesAllowed(USER)
    @Path("/{idCuenta}")
    public Response getBancoCuentaByIdCuenta(@PathParam("idCuenta") String idCuenta) {
        return Response.ok(bCtaRepos.findIdCuenta(idCuenta)).build();
    }
    
    @GET
    @Path("/test-dao")
    public List<Object[]> getTest() {
        return daoDatosMaestros.getAllDatos();
    }   
    
}
